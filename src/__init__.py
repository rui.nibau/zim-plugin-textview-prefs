'''Textview preferences module'''
import logging

from gi.repository import Gdk
from gi.repository.Gtk import CssProvider, STYLE_PROVIDER_PRIORITY_APPLICATION

from zim.gui.pageview import PageViewExtension
from zim.gui.widgets import widget_set_css
from zim.plugins import PluginClass
from zim.plugins.distractionfree import MaxWidth
from zim.signals import SIGNAL_AFTER

logger = logging.getLogger('zim.plugins.textviewprefs')

class TextviewPreferencesPlugin(PluginClass):
	'''Preference class'''

	plugin_info = {
		'name': _('Textview Preferences'), # T: plugin name
		'description': _('''Change some textview properties (margins, space between lines, text width).
			<https://omacronides.com/projets/zim-plugin-textview-prefs>'''), # T: plugin description
		'author': 'Rui Nibau',
	}

	plugin_preferences = (
		# key, type, label, default, range
		('maxwidth', 'int', _('Text max width'), 800, (0, 5000)),
		('padding', 'int', _('Padding around text'), 30, (0, 100)), # T: preferences option
		('wrapspacing', 'int', _('Space between Wrapped lines'), 5, (0, 100)), # T: preferences option
		# colors
		('background', 'color', _('Background color'), '#2c2c2c'), # T: plugin preference
		('color', 'color', _('Text color'), '#babdb6'), # T: plugin preference
		# config
		('override', 'bool', _('Override distraction Free'), False), # T: plugin preference
	)

class TextviewPreferencesPageViewExtension(PageViewExtension):
	'''Customize textview'''

	def __init__(self, plugin, window):
		PageViewExtension.__init__(self, plugin, window)
		self.preferences = plugin.preferences

		self.vpadding_provider = None
		self.hpadding_provider = None
		self.styles_provider = None

		self.current_width = None
		self.previous_line_height = None

		self.connectto(plugin.preferences, 'changed', self.on_preferences_changed)
		self.connectto(window, 'window-state-event', order=SIGNAL_AFTER)

		self.do_apply_preferences()

	def teardown(self):
		PageViewExtension.teardown(self)
		self.disconnect_all()
		self.do_remove_preferences()

	def _new_style_page_provider(self):
		# GTK css does not understand "border" shortand ?
		css = '''
		#zim-pageview,
		#zim-pageview text {
			color: %(color)s;
			caret-color: %(color)s;
			background-color: %(background)s;
			border-color: %(background)s;
			background-clip: border-box;
		}
		#zim-pageview text {
			background-color: transparent;
		}
		''' % self.preferences
		logger.debug('Plugin Textviewpreferences - styles: %s', css)
		provider = CssProvider()
		provider.load_from_data(css.encode('UTF-8'))
		return provider

	def _new_vpadding_provider(self, padding):
		'''Vertical padding for the scrolled widget'''
		css = f".frame {{padding: {padding}px 0px; background-color: {self.preferences['background']};}}"
		logger.debug('Plugin Textviewpreferences - vpadding: %s', css)
		provider = CssProvider()
		provider.load_from_data(css.encode('UTF-8'))
		return provider

	def _new_hpadding_provider(self, padding):
		'''Horizontal padding for the textview'''
		css = f"#zim-pageview {{padding: 0px {padding}px;}}"
		logger.debug('Plugin Textviewpreferences - hpadding: %s', css)
		provider = CssProvider()
		provider.load_from_data(css.encode('UTF-8'))
		return provider

	def on_preferences_changed(self):
		'''Prefreneces change listener'''
		self.do_apply_preferences()

	def on_window_state_event(self, window, event):
		'''Window state listener'''
		if bool(event.changed_mask & Gdk.WindowState.FULLSCREEN):
			self.on_fullscreen_changed(window)

	def on_fullscreen_changed(self, window):
		'''Fullscreen change listener'''
		if not self.preferences['override']:
			self.disconnect_from(self.pageview)
			if window.isfullscreen:
				self.do_remove_preferences()
			else:
				self.do_apply_preferences()

	def on_size_allocated(self, pageview, allocation):
		'''Allocation size listener'''
		if allocation.width == self.current_width:
			return
		self.current_width = allocation.width
		maxwidth = self.preferences['maxwidth']
		if self.current_width > maxwidth:
			self.do_apply_hpadding(max((self.current_width - maxwidth) / 2, self.preferences['padding']))

	def do_apply_preferences(self):
		'''Apply Prefreneces'''

		# Wrapspacing
		self.previous_line_height = self.pageview.textview.get_pixels_inside_wrap()
		self.pageview.textview.set_pixels_inside_wrap(self.preferences['wrapspacing'])

		# styles
		self.do_apply_styles()
		self.do_apply_vpadding(self.preferences['padding'])
		self.do_apply_hpadding(self.preferences['padding'])

		# max-width
		self.disconnect_from(self.pageview)
		if self.preferences['maxwidth'] > 0:
			self.connectto(self.pageview, 'size-allocate', self.on_size_allocated)

	def do_remove_preferences(self):
		'''Remove preferences'''
		self.disconnect_from(self.pageview)
		if self.previous_line_height is not None:
			self.pageview.textview.set_pixels_inside_wrap(self.previous_line_height)
		self.do_remove_styles()
		self.do_remove_vpadding()
		self.do_remove_hpadding()

	def do_apply_styles(self):
		'''Apply main styles'''
		self.do_remove_styles()
		style_context = self.pageview.textview.get_style_context()
		self.styles_provider = self._new_style_page_provider()
		style_context.add_provider(self.styles_provider, STYLE_PROVIDER_PRIORITY_APPLICATION)

	def do_remove_styles(self):
		'''Remove main styles'''
		if self.styles_provider is not None:
			style_context = self.pageview.textview.get_style_context()
			style_context.remove_provider(self.styles_provider)

	def do_apply_vpadding(self, padding):
		'''Apply vertical padding on scrolled'''
		self.do_remove_vpadding()
		style_context = self.pageview.textview.get_parent().get_style_context()
		self.vpadding_provider = self._new_vpadding_provider(padding)
		style_context.add_provider(self.vpadding_provider, STYLE_PROVIDER_PRIORITY_APPLICATION)

	def do_remove_vpadding(self):
		'''Remove vertical padding on scrolled'''
		if self.vpadding_provider is not None:
			style_context = self.pageview.textview.get_parent().get_style_context()
			style_context.remove_provider(self.vpadding_provider)

	def do_apply_hpadding(self, padding):
		'''Apply horizontal padding to the textview'''
		self.do_remove_hpadding()
		style_context = self.pageview.textview.get_style_context()
		self.hpadding_provider = self._new_hpadding_provider(padding)
		style_context.add_provider(self.hpadding_provider, STYLE_PROVIDER_PRIORITY_APPLICATION)

	def do_remove_hpadding(self):
		'''Remove horizontal padding to the textview'''
		if self.hpadding_provider is not None:
			self.pageview.textview.get_style_context().remove_provider(self.hpadding_provider)
