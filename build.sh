DIR="$( cd "$( dirname "$0" )" && pwd )"

TMP_DIR=$DIR"/tmp/"
SRC_DIR=$DIR"/src/"
BUILD_DIR=$DIR"/build/"

APPNAME="textview-prefs"
VERSION="2.0.4"

build() {

    local arch=$DIR"/build/"$APPNAME"-"$VERSION".zip"

    echo "Building "$APPNAME

    # clean
    echo "cleaning..."
    rm -fr $TMP_DIR$APPNAME
    # Supprimer l'archive si elle existe déjà
    if [[ -f $arch ]]; then
        rm $arch
    fi
    # build
    echo "Copying..."
    mkdir $TMP_DIR$APPNAME
    cp -p $DIR"/src/__init__.py" $TMP_DIR$APPNAME
    cp -prL $DIR"/doc" $TMP_DIR$APPNAME"/doc"
    # zip
    echo "Zipping..."
    cd $TMP_DIR
    zip -r -FS $arch $APPNAME
    cp $arch $DIR"/build/"$APPNAME"-latest.zip"
    echo "Done."
}

# run
build
