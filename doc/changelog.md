---
title:      Zim Textview Prefs : Historique
date:       2019-02-07
updated:    2024-04-29
---

°°changelog°°
next:
    • upd: [3](https://framagit.org/rui.nibau/zim-plugin-textview-prefs/-/issues/3). Equivalent to javascript debounce
2.0.4 øø(2024-04-28)øø:
    • fix: [2](https://framagit.org/rui.nibau/zim-plugin-textview-prefs/-/issues/2). apply to new windows.
2.0.3 øø(2024-03-21)øø:
    • fix: [1](https://framagit.org/rui.nibau/zim-plugin-textview-prefs/-/issues/1). cannot load plugin 2.0.2
2.0.2 øø(2024-02-21)øø:
    • fix: remove preferences
2.0.1 øø(2024-02-20)øø:
    • fix: frame background
2.0.0 øø(2024-02-19)øø:
    • add: max-width
    • upd: rollback on destroy
    • upd: clean code
    • del: obsolete configuration
1.1.0 øø(2019-04-22)øø:
    • fix: 0002: don't override fullscreen preferences.
    • fix: cursor color with dark background.
1.0.0 øø(2019-02-07)øø:
    • add: First version.

