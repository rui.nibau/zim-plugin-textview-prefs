---
title:      Zim plugin : Textview prefs
banner:     /lab/zim-plugin-textview-prefs/pics/banner.png
cats:       [informatique]
tags:       [zim, extensions, configuration]
techs:      [python]
date:       2019-02-07
updated:    2024-04-29
version:    2.0.4
status:     in-process
itemtype:   SoftwareApplication
pagesAsSnippets: true
source:     https://framagit.org/rui.nibau/zim-plugin-textview-prefs
issues:     https://framagit.org/rui.nibau/zim-plugin-textview-prefs/-/issues
download:   /lab/zim-plugin-textview-prefs/build/textview-prefs-latest.zip
intro: An extremely simple plugin to customize [Zim](/articles/zim/)'s textview.
---

## About

These are customizations of the zim page textview that I make each time I install Zim through a patch of the source code. But the plugin is a better form of diffusion. So here it is. To install it, just unzip the archive in ``~/.local/zim/plugins/``.

°°pos-left sz-half splash°°
----
![Without plugin](/lab/zim-plugin-textview-prefs/pics/without.png)
----
Zim without the plugin

°°pos-right sz-half splash°°
----
![Without plugin](/lab/zim-plugin-textview-prefs/pics/with.png)
----
Zim with the plugin

## Configuration

°°pic pos-right sz-half°°
![Textview prefs 2.0.0](/lab/zim-plugin-textview-prefs/pics/2.0-config.png)

Text max width:
    Limit the width of the text for better reading.
Padding around text:
    Draws paddings/margins around textview (top, right, bottom, left).
Space between wrapped lines:
    Define space for wrapped lines. What we call « [line-height](https://developer.mozilla.org/docs/Web/CSS/line-height) » in web applications and that it is just partially rendered by the « linespacing » property of style.conf.
Background color:
Text color:
    Colors for the text and the background of the textview.
Override distraction Free:
    Keep those settings when distaction free editor is active.

## References

• [Writting plugins for Zim](https://github.com/zim-desktop-wiki/zim-desktop-wiki/blob/master/PLUGIN_WRITING.md)
• [Discussion on github](https://github.com/zim-desktop-wiki/zim-desktop-wiki/discussions/2576)

## History

..include::./changelog.md


## Licence

..include::./licence.md


